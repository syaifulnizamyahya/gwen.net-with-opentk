﻿// FPS camera based on this sample http://www.opentk.com/node/3756
// 
namespace GwenFpsCamera
{
	class FirstPersonCamera
	{
		public OpenTK.Vector3 Position;
		public OpenTK.Vector3 Rotation;
		public OpenTK.Quaternion Orientation;

		public OpenTK.Matrix4 Matrix;
		public OpenTK.Matrix4 Model;
		public OpenTK.Matrix4 Projection;

		public FirstPersonCamera()
		{
			Matrix = OpenTK.Matrix4.Identity;
			Projection = OpenTK.Matrix4.Identity;
			Orientation = OpenTK.Quaternion.Identity;
		}

		public void Update()
		{
			Orientation =
				OpenTK.Quaternion.FromAxisAngle(OpenTK.Vector3.UnitY, Rotation.Y) *
				OpenTK.Quaternion.FromAxisAngle(OpenTK.Vector3.UnitX, Rotation.X);

			var forward = OpenTK.Vector3.Transform(OpenTK.Vector3.UnitZ, Orientation);
			Model = OpenTK.Matrix4.LookAt(Position, Position + forward, OpenTK.Vector3.UnitY);
			Matrix = Model * Projection;
		}

		public void Resize(int width, int height)
		{
			Projection = OpenTK.Matrix4.CreatePerspectiveFieldOfView(
				OpenTK.MathHelper.PiOver4, (float)width / height, 0.1f, 1000f
			);
		}

		public void TurnX(float a)
		{
			Rotation.X += a;
			Rotation.X = OpenTK.MathHelper.Clamp(Rotation.X, -1.57f, 1.57f);
		}

		public void TurnY(float a)
		{
			Rotation.Y += a;
			Rotation.Y = ClampCircular(Rotation.Y, 0, OpenTK.MathHelper.TwoPi);
		}

		public void MoveX(float a)
		{
			Position += OpenTK.Vector3.Transform(OpenTK.Vector3.UnitX * a, OpenTK.Quaternion.FromAxisAngle(OpenTK.Vector3.UnitY, Rotation.Y));
		}

		public void MoveY(float a)
		{
			Position += OpenTK.Vector3.Transform(OpenTK.Vector3.UnitY * a, OpenTK.Quaternion.FromAxisAngle(OpenTK.Vector3.UnitY, Rotation.Y));
		}

		public void MoveZ(float a)
		{
			Position += OpenTK.Vector3.Transform(OpenTK.Vector3.UnitZ * a, OpenTK.Quaternion.FromAxisAngle(OpenTK.Vector3.UnitY, Rotation.Y));
		}

		public void MoveYLocal(float a)
		{
			Position += OpenTK.Vector3.Transform(OpenTK.Vector3.UnitY * a, Orientation);
		}

		public void MoveZLocal(float a)
		{
			Position += OpenTK.Vector3.Transform(OpenTK.Vector3.UnitZ * a, Orientation);
		}

		public static float ClampCircular(float n, float min, float max)
		{
			if (n >= max) n -= max;
			if (n < min) n += max;
			return n;
		}
	}

	class GLDraw
	{
		static GLDraw()
		{
		}

		public static void Axis()
		{
			OpenTK.Graphics.OpenGL.GL.LineWidth(2f);
			OpenTK.Graphics.OpenGL.GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Lines);

			OpenTK.Graphics.OpenGL.GL.Color3(1f, 0f, 0f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(0f, 0f, 0f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(1f, 0f, 0f);

			OpenTK.Graphics.OpenGL.GL.Color3(0f, 1f, 0f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(0f, 0f, 0f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(0f, 1f, 0f);

			OpenTK.Graphics.OpenGL.GL.Color3(0f, 0f, 1f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(0f, 0f, 0f);
			OpenTK.Graphics.OpenGL.GL.Vertex3(0f, 0f, 1f);

			OpenTK.Graphics.OpenGL.GL.End();
			OpenTK.Graphics.OpenGL.GL.LineWidth(1f);
		}

		public static void WireCube(float size = 0.5f)
		{
			// Bottom
			OpenTK.Graphics.OpenGL.GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.LineLoop);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, -size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, -size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, -size, size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, -size, size);
			OpenTK.Graphics.OpenGL.GL.End();

			// Top
			OpenTK.Graphics.OpenGL.GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.LineLoop);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, size, size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, size, size);
			OpenTK.Graphics.OpenGL.GL.End();

			// Vertical
			OpenTK.Graphics.OpenGL.GL.Begin(OpenTK.Graphics.OpenGL.PrimitiveType.Lines);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, -size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, -size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, size, -size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, -size, size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(size, size, size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, -size, size);
			OpenTK.Graphics.OpenGL.GL.Vertex3(-size, size, size);
			OpenTK.Graphics.OpenGL.GL.End();
		}

		public static void Grid(float iterations = 10, float space = 5)
		{
			OpenTK.Graphics.OpenGL.GL.Color3(1f, 1f, 1f);

			var gridBound = iterations * space;

			OpenTK.Graphics.OpenGL.GL.Begin(OpenTK.Graphics.OpenGL.BeginMode.Lines);

			for (var x = -iterations; x <= iterations; x++)
			{
				OpenTK.Graphics.OpenGL.GL.Vertex3(x * space, 0, -gridBound);
				OpenTK.Graphics.OpenGL.GL.Vertex3(x * space, 0, gridBound);

				for (var z = -iterations; z <= iterations; z++)
				{
					OpenTK.Graphics.OpenGL.GL.Vertex3(-gridBound, 0, z * space);
					OpenTK.Graphics.OpenGL.GL.Vertex3(gridBound, 0, z * space);
				}
			}

			OpenTK.Graphics.OpenGL.GL.End();
		}
	}

}
