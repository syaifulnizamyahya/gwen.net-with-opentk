﻿// http://www.opentk.com/node/3756
// 
using System;
using System.Drawing;

namespace GwenFpsCamera
{
	/// <summary>
	/// Main 
	/// </summary>
	public class GwenGameWindow : OpenTK.GameWindow
	{
		private Gwen.Input.OpenTK _input;
		private Gwen.Renderer.OpenTK _renderer;
		private Gwen.Skin.Base _skin;
		private Gwen.Control.Canvas _canvas;

		private FirstPersonCamera _camera;
		private OpenTK.Vector2 _mouseMove;
		private float _mouseSensitivity = 0.1f;
		private float _moveSpeed = 5;

		private OpenTK.Input.MouseState _currentMouseState;
		private OpenTK.Input.MouseState _previousMouseState;
		private OpenTK.Input.MouseState _previousMenuMouseState;

		private bool _mainMenu = true;

		/// <summary>
		/// Main constructor
		/// </summary>
		/// <returns></returns>
		public GwenGameWindow()
		{
			Width = 1024;
			Height = 768;
			Keyboard.KeyDown += Keyboard_KeyDown;
			Keyboard.KeyUp += Keyboard_KeyUp;

			Mouse.ButtonDown += Mouse_ButtonDown;
			Mouse.ButtonUp += Mouse_ButtonUp;
			Mouse.Move += Mouse_Move;
			Mouse.WheelChanged += Mouse_Wheel;
		}

		public override void Dispose()
		{
			_canvas.Dispose();
			_skin.Dispose();
			_renderer.Dispose();
			base.Dispose();
		}

		/// <summary>
		/// Occurs when a key is pressed.
		/// </summary>
		/// <param name="sender">The KeyboardDevice which generated this event.</param>
		/// <param name="e">The key that was pressed.</param>
		void Keyboard_KeyDown(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			if (e.Key == global::OpenTK.Input.Key.Escape)
			{
				_mainMenu = !_mainMenu;
				_previousMouseState = OpenTK.Input.Mouse.GetState();

				if (_mainMenu)
				{
					OpenTK.Input.Mouse.SetPosition(_previousMenuMouseState.X, _previousMenuMouseState.Y);
					this.CursorVisible = true;
				}
				else
				{
					_previousMenuMouseState = OpenTK.Input.Mouse.GetCursorState();
					this.CursorVisible = false;
				}
			}

			_input.ProcessKeyDown(e);
		}

		void Keyboard_KeyUp(object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			_input.ProcessKeyUp(e);
		}

		void Mouse_ButtonDown(object sender, OpenTK.Input.MouseButtonEventArgs args)
		{
			_input.ProcessMouseMessage(args);
		}

		void Mouse_ButtonUp(object sender, OpenTK.Input.MouseButtonEventArgs args)
		{
			_input.ProcessMouseMessage(args);
		}

		void Mouse_Move(object sender, OpenTK.Input.MouseMoveEventArgs args)
		{
			_input.ProcessMouseMessage(args);
		}

		void Mouse_Wheel(object sender, OpenTK.Input.MouseWheelEventArgs args)
		{
			_input.ProcessMouseMessage(args);
		}

		/// <summary>
		/// Setup OpenGL and load resources here.
		/// </summary>
		/// <param name="e">Not used.</param>
		protected override void OnLoad(EventArgs e)
		{
			OpenTK.Graphics.OpenGL.GL.Enable(OpenTK.Graphics.OpenGL.EnableCap.DepthTest);
			OpenTK.Graphics.OpenGL.GL.ClearColor(Color.Black);

			// Initialize gwen
			// 1. create renderer
			_renderer = new Gwen.Renderer.OpenTK();

			// 2. create skin
			_skin = new Gwen.Skin.TexturedBase(_renderer, "DefaultSkin.png");
			_skin.DefaultFont = new Gwen.Font(_renderer, "Courier", 10);

			// 3. create canvas
			_canvas = new Gwen.Control.Canvas(_skin);
			_canvas.ShouldDrawBackground = true;
			_canvas.BackgroundColor = Color.FromArgb(128, 128, 128, 128);

			// 4. create input
			_input = new Gwen.Input.OpenTK(this);
			_input.Initialize(_canvas);

			// 5. create ui element
			var button = new Gwen.Control.Button(_canvas);
			button.SetText("Hello world");

			// initialize camera
			_camera = new FirstPersonCamera();
			// position the camera 
			_camera.Position = new OpenTK.Vector3(0, 1, -5);
		}

		/// <summary>
		/// Respond to resize events here.
		/// </summary>
		/// <param name="e">Contains information on the new GameWindow size.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnResize(EventArgs e)
		{
			// camera resize
			_camera.Resize(Width, Height);
			_camera.Update();

			// gwen resize
			OpenTK.Graphics.OpenGL.GL.Viewport(0, 0, Width, Height);
			OpenTK.Graphics.OpenGL.GL.MatrixMode(OpenTK.Graphics.OpenGL.MatrixMode.Projection);
			OpenTK.Graphics.OpenGL.GL.LoadIdentity();
			OpenTK.Graphics.OpenGL.GL.Ortho(0, Width, Height, 0, -1, 1);

			_canvas.SetSize(Width, Height);
		}

		/// <summary>
		/// Add your game logic here.
		/// </summary>
		/// <param name="e">Contains timing information.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnUpdateFrame(OpenTK.FrameEventArgs e)
		{
			// each cached string is an allocated texture, 
			// flush the cache once in a while in your real project
			if (_renderer.TextCacheSize > 1000)
			{
				_renderer.FlushTextCache();
			}

			// update the camera if were not drawing the main menu
			if (!_mainMenu)
			{
				var time = (float)e.Time;
				var move = _moveSpeed * time;

				// get current mouse position
				_currentMouseState = OpenTK.Input.Mouse.GetState();
				// warp mouse to the center (not really). so that the cursor 
				// would not go out of bounds.
				OpenTK.Input.Mouse.SetPosition(Width / 2, Height / 2);

				// check if the mouse moved. if it is, move the camera accordingly
				if (_currentMouseState.X != _previousMouseState.X)
				{
					_mouseMove.Y = (_currentMouseState.X - _previousMouseState.X) *
						_mouseSensitivity * time;
				}
				else
				{
					_mouseMove.Y = 0;
				}

				if (_currentMouseState.Y != _previousMouseState.Y)
				{
					_mouseMove.X = (_currentMouseState.Y - _previousMouseState.Y) *
						_mouseSensitivity * time;
				}
				else
				{
					_mouseMove.X = 0;
				}
				// save current mouse position 
				_previousMouseState = _currentMouseState;

				// move the camera 
				_camera.TurnX(_mouseMove.X);
				_camera.TurnY(-_mouseMove.Y);

				// camera movement by keyboard
				if (Keyboard[OpenTK.Input.Key.A])
				{
					_camera.MoveX(move);
				}
				if (Keyboard[OpenTK.Input.Key.D])
				{
					_camera.MoveX(-move);
				}
				if (Keyboard[OpenTK.Input.Key.Space])
				{
					_camera.MoveY(move);
				}
				if (Keyboard[OpenTK.Input.Key.ControlLeft])
				{
					_camera.MoveY(-move);
				}
				if (Keyboard[OpenTK.Input.Key.W])
				{
					_camera.MoveZ(move);
				}
				if (Keyboard[OpenTK.Input.Key.S])
				{
					_camera.MoveZ(-move);
				}

				_camera.Update();
			}
		}

		/// <summary>
		/// Add your game rendering code here.
		/// </summary>
		/// <param name="e">Contains timing information.</param>
		/// <remarks>There is no need to call the base implementation.</remarks>
		protected override void OnRenderFrame(OpenTK.FrameEventArgs e)
		{
			OpenTK.Graphics.OpenGL.GL.Clear(OpenTK.Graphics.OpenGL.ClearBufferMask.DepthBufferBit | OpenTK.Graphics.OpenGL.ClearBufferMask.ColorBufferBit);
			OpenTK.Graphics.OpenGL.GL.LoadMatrix(ref _camera.Matrix);

			// draw stuff here
			GLDraw.Grid();
			GLDraw.Axis();

			if (_mainMenu)
			{
				RenderMainMenu();
			}

			SwapBuffers();
		}

		private void RenderMainMenu()
		{
			// 6. render gwen ui
			OpenTK.Graphics.OpenGL.GL.Viewport(0, 0, Width, Height);
			OpenTK.Graphics.OpenGL.GL.MatrixMode(OpenTK.Graphics.OpenGL.MatrixMode.Projection);
			OpenTK.Graphics.OpenGL.GL.LoadIdentity();
			OpenTK.Graphics.OpenGL.GL.Ortho(0, Width, Height, 0, -1, 1);
			_canvas.RenderCanvas();
		}

		/// <summary>
		/// Entry point of this example.
		/// </summary>
		[STAThread]
		public static void Main()
		{
			using (var gwenGameWindow = new GwenGameWindow())
			{
				gwenGameWindow.Run();
			}
		}
	}
}
