# README #

This repositories contains programming samples that uses
1. OpenTK (for OpenGL in C#)
2. Gwen.Net (for UI)

## Project overview ###

### 1. GwenFpsCamera ###
FPS camera with Gwen.Net GUI. FPS camera based on this [http://www.opentk.com/node/3756](http://www.opentk.com/node/3756)

Press escape to change between 3d mode and menu mode. 

####In 3d mode:####
Use W,A,S,D,Space,Left Ctrl to move. 

Use mouse to look.

####In menu mode:####
Use mouse to interact.

####To add menu content:####
Add Gwen gui element at line 133, Program.cs

####To add object to 3d space:####
Add 3d object at line 256, Program.cs